#!/usr/bin/env python
# coding: utf-8

import pprint
import requests
import math
import sys
import xmltodict
from PIL import Image
import StringIO
import urllib
from geopy.distance import vincenty
from attenuation import horizontal
from attenuation import vertical

def calculate_azimuth(pointA, pointB):
    """
    from: https://gist.github.com/jeromer/2005586
    Calculates the bearing between two points.
    The formulae used is the following:
    :Parameters:
      - `pointA: The tuple representing the latitude/longitude for the
        first point. Latitude and longitude must be in decimal degrees
      - `pointB: The tuple representing the latitude/longitude for the
        second point. Latitude and longitude must be in decimal degrees
    :Returns:
      The bearing in degrees
    :Returns Type:
      float
    """
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")
    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])
    diffLong = math.radians(pointB[1] - pointA[1])
    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
            * math.cos(lat2) * math.cos(diffLong))
    initial_bearing = math.atan2(x, y)
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360
    return compass_bearing

def calculate_middle(pointA, pointB):
    return (((pointA[0] + pointB[0]) / 2.), ((pointA[1] + pointB[1]) / 2.))

def csv2arr(p):
    sites = open(p, "rU").read()
    ret = []
    for line in sites.split("\n"):
        if line == "":
            continue
        ret.append(line.split(";"))
    return ret

def new_latitude(lat, dist):
    # 6378 == earth radius
    return lat + (dist / 6378) * (180 / math.pi)

def new_longitude(long, lat, dist):
    # 6378 == earth radius
    return long + (dist / 6378) * (180 / math.pi) / math.cos(lat * math.pi / 180)

def find_max_min_long_lat(antennes):
    min_long = new_longitude(antennes[0]["lon"], antennes[0]["lat"], -(abs(antennes[0]["radius"] / 1000)))
    max_long = new_longitude(antennes[0]["lon"], antennes[0]["lat"], abs(antennes[0]["radius"] / 1000))
    min_lat = new_latitude(antennes[0]["lat"], -(abs(antennes[0]["radius"] / 1000)))
    max_lat = new_latitude(antennes[0]["lat"], abs(antennes[0]["radius"] / 1000))
    for a in antennes[1:]:
        min_long = min(min_long, new_longitude(a["lon"], a["lat"], -(abs(a["radius"] / 1000))))
        max_long = max(max_long, new_longitude(a["lon"], a["lat"], abs(a["radius"] / 1000)))
        min_lat = min(min_lat, new_latitude(a["lat"], -(abs(a["radius"] / 1000))))
        max_lat = max(max_lat, new_latitude(a["lat"], abs(a["radius"] / 1000)))
    return (min_long, max_long, min_lat, max_lat)

def create_antenna(line, arr):
    long = new_longitude(float(line[1]), float(line[2]), float(arr[7]) / 1000.)
    lat = new_latitude(float(line[2]), float(arr[8]) / 1000.)
    return { "lon": long,
            "lat": lat,
            "height": float(arr[9]),
            "azimuth": float(arr[4]), 
            "radius": float(arr[12]),
            "erp": float(arr[6])
            }

# building = [ (lat, lon) ]

def building_inside_antenna(b, a):
    # True if a part of the building is inside the antenna radius, False otherwise
    for dot in b:
        m = vincenty(dot, (a["lat"], a["lon"])).meters
        if m <= a["radius"]:
            return True
    return False

def building_inside_any_antenna(b, ants):
    for a in ants:
        if building_inside_antenna(b, a):
            return True
    return False

def clean_buildings_list(blst, ants):
    nl = []
    for b in blst:
        if building_inside_any_antenna(b, ants):
            nl.append(b)
    return nl

def formula(lat, lon, ants):
    e = []
    for a in ants:
        azim = calculate_azimuth((a["lat"], a["lon"]), (lat,lon))
        diffazim = (int(azim - a["azimuth"]) + 360) % 360
        horiz_att = horizontal[diffazim]
        if horiz_att > 15: horiz_att = 15
        e.append(((7 / vincenty((lat, lon), (a["lat"], a["lon"])).meters) *
            (a["erp"] / (1. * (horiz_att + vertical[0]))) ** .5))
    return sum([i * i for i in e])**.5

    

def get_energy_sum(d, ants):
    en_sum = []
    for lat, lon in d:
        en_sum.append((formula(lat, lon, ants), (lat, lon)))
    return sorted(en_sum)[-2:]

def solve_building(b, ants):
    d = []
    for a in ants:
        adist = []
        for coord in b:
            adist.append((vincenty(coord, (a["lat"], a["lon"])).meters, coord))
        adist = sorted(adist)
        d += [ adist[0][1], adist[1][1] ]
    d = list(set(d))
    en_sum = get_energy_sum(d, ants)
    while vincenty(en_sum[0][1], en_sum[1][1]).meters > 1.:
        mid = calculate_middle(en_sum[0][1], en_sum[1][1])
        en_sum[0] = (formula(mid[0], mid[1], ants), (mid[0], mid[1]))
        en_sum = sorted(en_sum)
    return en_sum[-1]

def set_omen(i, sol, site):
    x = vincenty((sol[1][0], sol[1][1]), (float(site[2]), sol[1][1])).meters
    if sol[1][0] < float(site[2]): x = -x
    y = vincenty((sol[1][0], sol[1][1]), (sol[1][0], float(site[1]))).meters
    if sol[1][1] < float(site[1]): y = -y
    return { "OMEN_ID": i,
            "lat": sol[1][0],
            "lon": sol[1][1],
            "X": int(x),
            "Y": int(y),
            "Z": "UNKNOWN ATM",
            "Efield": sol[0],
            "Approved": "Yes" if sol[0] < 5. else "No"
            }

def solve(blst, ants, site):
    csv_out = [] # x / y / z / Efield
    for i, b in enumerate(blst):
        sol = solve_building(b, ants)
        csv_out.append(set_omen(i, sol, site))
    return csv_out

def gps2tile(lat, lon, z):
    latRad = lat * math.pi / 180;
    n = math.pow(2, z);
    xTile = n * ((lon + 180) / 360);
    yTile = n * (1-(math.log(math.tan(latRad) + 1/math.cos(latRad)) /    math.pi)) / 2;
    return xTile, yTile

def str2Image(c):
    tempBuff = StringIO.StringIO()
    tempBuff.write(c)
    tempBuff.seek(0)
    image = Image.open(tempBuff)
    return image

def antennas2markers(ants):
    s = "&markers=color:blue"
    for a in ants:
        s += "|%f,%f" % (a["lat"], a["lon"])
    #return urllib.quote_plus(s)
    return s

def hots2markers(hots):
    red = "&markers=color:red"
    gre = "&markers=color:green"
    for e in hots:
        if e["Efield"] < 5.:
            gre += "|%f,%f" % (e["lat"], e["lon"])
        else:
            red += "|%f,%f" % (e["lat"], e["lon"])
    #return urllib.quote_plus(s)
    return red + gre

def getmap(site, ants, hots, zoom = 18):
    antsmarks = antennas2markers(ants)
    print antsmarks
    hotsmarks = hots2markers(hots)
    print hotsmarks
    print site
    r = requests.get("https://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=%d&size=1200x1200&key=AIzaSyCCC_AJ6b40ZjerH0jykOGPsO5MrIOVwAw%s%s"
        % (float(site[2]), float(site[1]), zoom, antsmarks, hotsmarks))
    print r
    c = r.content
    str2Image(c).show()

def write_csv_out(csv_out, site):
    fd = open(site[0] + "_Omen_out.csv", "w")
    fd.write("OMEN_ID;X;Y;Z;Efield;Approved\n")
    for e in csv_out:
        fd.write(str(e["OMEN_ID"])+";"+str(e["X"])+";"+str(e["Y"])+";"+e["Z"]+";"+str(e["Efield"])+";"+e["Approved"]+"\n")

def main():
    antennes = []
    buildings = []
    site = []
    sites = csv2arr(sys.argv[1])
    for i, line in enumerate(sites[1:]):
        if i < 0: continue
        print line
        site = line
        for antenna in csv2arr("data/" + line[0] + "_Antennes.csv")[1:]:
            antennes.append(create_antenna(line, antenna))
        break
    for a in antennes:
        print a
    a,b,c,d = find_max_min_long_lat(antennes)
    print str(a)+","+str(c)+","+str(b)+","+str(d)
    data = requests.get("http://overpass-api.de/api/map?bbox="+str(a)+","+str(c)+","+str(b)+","+str(d)).content
    x = xmltodict.parse(data, dict_constructor=dict)
    for way in x["osm"]["way"]:
        is_building = False
        if "tag" in way.keys():
            if isinstance(way["tag"], dict):
                if way["tag"]["@k"] == "building" and way["tag"]["@v"] == "yes":
                    if way["tag"]["@v"] != "yes": print way["tag"]
                    is_building = True
            else:
                for tag in way["tag"]:
                    if tag["@k"] == "building" and tag["@v"] == "yes":
                        is_building = True
                        break
        if is_building:
            new_building = []
            for nd in way["nd"]:
                for node in x["osm"]["node"]:
                    if nd["@ref"] == node["@id"]:
                        new_building.append((float(node["@lat"]), float(node["@lon"])))
                        break
            if new_building[0] == new_building[-1]:
                new_building = new_building[:-1]
            if len(new_building) > 2:
                buildings.append(new_building)
    buildings = clean_buildings_list(buildings, antennes)
    csv_out = solve(buildings, antennes, site)
    getmap(site, antennes, csv_out)
    write_csv_out(csv_out, site)
    pass

if __name__ == "__main__":
    main()
